# Colorscheme 
[GruvBox Material Theme](https://marketplace.visualstudio.com/items?itemName=sainnhe.gruvbox-material)

[Edge](https://marketplace.visualstudio.com/items?itemName=sainnhe.edge)

# Extentions
[Thunder Client](https://marketplace.visualstudio.com/items?itemName=rangav.vscode-thunder-client) as a lightweight postman alternative.

# Reseting Pane sizes (of sidebar and terminal)

Move the cursor till the option to resize comes up, and then just double click! and it will resize!

[Learnt from here](https://dev.to/entrptaher/vscode-trick-reset-the-sidebar-and-terminal-pane-size-5cd1)

# Using the Native Title Bar

You can use the native title bar of each platform (Is set as true by default on Linux).

To do this go to preferences -> `window: title bar style` and set it to native.

You can also set this in `settings.json` with `"window.titleBarStyle": "native"`
